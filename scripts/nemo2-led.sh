#!/bin/sh
#
# Nemo2 LED Script
#
# Distributed under the MIT License, see LICENSE for details.
# 
# Usage: nemo2-led.sh <start|stop>
#   start: Sets GPIO high, waits 2 sec, then sets it to low and exits
#   stop: Sets GPIO high and exits
#
APP=nemo2-led.sh
#GPIO=22  # Physical pin 15 on the RPi
GPIO=17  # Physical pin 11 on the RPi
DEV=/sys/class/gpio
USAGE="Usage: $APP <start|stop>"

if [ -z $1 ]; then
    echo $USAGE
    exit 1
fi

# Enable GPIO and set to OUT
echo $GPIO > $DEV/export
echo out > $DEV/gpio$GPIO/direction

# Always set to high
echo 1 > $DEV/gpio$GPIO/value

# Perform additional actions
if [ $1_ = "start"_ ]; then
    sleep 2
    echo 0 > $DEV/gpio$GPIO/value
    exit 0
elif [ $1_ = "stop"_ ]; then
    exit 0
else
    echo "$0: invalid command"
    echo $USAGE
    exit 1
fi
