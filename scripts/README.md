INTRODUCTION
============
This guide describes how to set up the interaction
between the Raspberry Pi and the Led Control Module.

It is assumed you can SSH to your Volumio device and 
become root, as described here:

[https://volumio.github.io/docs/User_Manual/SSH.html    
]()

The guide assumes you access the deivce as user 'volumio'

The pin used to control the LCM is declared in the
GPIO variable in nemo2-led.sh.

Note that this value corresponds to the BCM GPIO pin, and
*not* to the physical pin!. For example, BCM.GPIO17 corresponds to physical pin 11.

See: [https://www.raspberrypi.org/documentation/usage/gpio/]() for a list of pinouts.


INSTRUCTIONS
============
- use `scp(1)` to copy the contents of this directory
  to /tmp on your Volumio device.
- login to your Volumio device and do:

```
$ cd 
$ mkdir scripts
$ cp /tmp/nemo2-led.sh scripts
$ chmod +x nemo2-led.sh
$ cd /lib/systemd/system
$ sudo cp /tmp/nemo2*.service .
$ sudo systemctl enable nemo2-startup.service
$ sudo systemctl enable nemo2-shutdown.service
$ sudo reboot
```

That's it!
