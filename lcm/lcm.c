/*
 * Copyright (c) 2020 Maarten Los
 *
 * Distributed under the MIT License, see LICENSE for details.
 * 
 * Firmware for the Nemo2 Led Control Module.
 *
 * Designed for an ATTiny85 running at 1 MHz.
 */
#include <avr/sleep.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "ufm.h"

/* ---------------- Enums ---------------- */

typedef enum
{
    starting,
    started,
    stopping,
    stopped,
} state_t;


/* ---------------- Types ---------------- */

#define STATES 4

typedef void (*state_func_t)();


typedef struct 
{
    /*
     * Index is current state, value is next state
     */
    state_t next_state[STATES];

    /*
     * All states
     */
    state_func_t states[STATES];

} state_machine_t;



/* ---------------- Global vars ---------------- */

volatile int g_state;
volatile int g_timer_overflows = 0;
volatile int g_overflows_before_timeout;
volatile state_machine_t g_state_machine;

/* ---------------- ISRs---------------- */

ISR (INT0_vect)
{
    transition_to_state(g_state_machine.next_state[g_state]);
}


ISR (TIMER1_OVF_vect)
{
    g_timer_overflows++;
    if(g_timer_overflows > g_overflows_before_timeout) {
        init_timer_overflow_interrupt(0);
           transition_to_state(stopped);
    } 
}


/* -------------- State Machine -------------- */

void stm_state_starting()
{
    set_led(none);
    set_pwm(led1, 40, 20);
    init_int0_interrupt(rising);

    /* 
     * Enable overflow interrupt which uses a counter
     * to trigger state -> stopped state on timeout.
     *
     * Disabled when moving to next state within N seconds.
     *
     * Given N=60, with 16ms per cycle and
     *     40 cycles for each overflow yields:
     * 60000 / (16 * 40) ~ 94 overflow interrupts
     */
    g_overflows_before_timeout = 94;
    init_timer_overflow_interrupt(1);
}


void stm_state_started()
{
    init_timer_overflow_interrupt(0);
    set_pwm(none, 0, 0);
    set_led(led1);
    init_int0_interrupt(rising);
}


void stm_state_stopping()
{
    set_led(none);
    set_pwm(led2, 20, 10);
    init_int0_interrupt(falling);
}

void stm_state_stopped()
{
    set_led(none);
    set_pwm(led2, 200, 20);
}


void init_state_machine()
{
    g_state_machine.next_state[starting] = started;
    g_state_machine.states[starting] = stm_state_starting;

    g_state_machine.next_state[started] = stopping;
    g_state_machine.states[started] = stm_state_started;

    g_state_machine.next_state[stopping] = stopped;
    g_state_machine.states[stopping] = stm_state_stopping;

    g_state_machine.next_state[stopped] = stopped;
    g_state_machine.states[stopped] = stm_state_stopped;
}
        
/* -------------- Interrupt initialization ------------ */

void init_int0_interrupt(sense_t sense)
{
    cli();

    if(sense == rising) {
        MCUCR = _BV(ISC00) | _BV(ISC01);
    } else if(sense == falling) {
        MCUCR = _BV(ISC01);
    }

    if(sense != disabled) {
        GIMSK |= _BV(INT0);
    } else {
        GIMSK &= ~_BV(INT0);
    }

    sei();
}

void init_timer_overflow_interrupt(char enable)
{
    if(enable) {
        TIMSK |= _BV(TOIE1);
    } else {
        TIMSK &= _BV(TOIE1);
    }
}
    

/* -------------- PWM Handling ---------------------*/

/*
 * Initializes basic PWM settings
 * 
 */
void init_pwm()
{
    /* 
     * Set to slowest, as we want visible blinking
     * of LEDs in PWM
     *
     * Frequency = 1MHz / 16384 ~ 60 Hz
     *
     * 1 tick ~ 16ms
     */   
    TCCR1 = _BV(CS13) | _BV(CS12) | _BV(CS11) | _BV(CS10);

    /*
     * Use OCR1C for precise blinking control
     */
    TCCR1 |= _BV(CTC1);
}

/*
 * Sets runtime PWM parameters for LED blinking.
 * 
 * A tick is 16ms.
 *
 * @param led Selects the active LED. 
 *        led1 enables LED mapped to PB1.
 *        led2 enabled LED mapped to PB4.
 *        none disables all LEDs and PWM.
 *        Enabling a LED automatically disables
 *        PWM on the other LED.
 * @param ticks_total The number of total ticks 
 *        for each LED cycle.
 *        Ignored if led is none.
 * @param ticks_on The number of ticks the LED should
 *        be on during each LED cycle.
 *        Ignored if led is none.
 * 
 */
void set_pwm(led_t led, int ticks_total, int ticks_on)
{
    OCR1C = ticks_total;

    /* All PWM OFF */
    TCCR1 &= ~(_BV(PWM1A) | _BV(COM1A1));
    GTCCR &= ~(_BV(PWM1B) | _BV(COM1B1));
    if(led == none) {
        return;
    } else if(led == led1) {
        TCCR1 |= _BV(PWM1A) | _BV(COM1A1);
        OCR1A = ticks_on;
    } else if(led == led2) {
        GTCCR |= _BV(PWM1B) | _BV(COM1B1);
        OCR1B = ticks_on;
    }
}


/* -------------- I/O -------------- */

void init_ports()
{
    /* Enable ports PB1 (OC1A) and PB4 (OC1B) */
    DDRB |= _BV(DDB1) | _BV(DDB4);
}


/*
 * Set either led1, led2 or none of them
 */
void set_led(led_t led)
{
    /* All LEDs OFF */
    PORTB &= ~(_BV(PORTB1) | _BV(PORTB4));

    if(led == none)
    {
        return;
    } else if(led == led1) {
        PORTB |= _BV(PORTB1);
    } else if(led == led2) {
        PORTB |= _BV(PORTB4);
    }
}

/* -------------- Main Logic -------------- */
 

void transition_to_state(int state)
{
    g_state = state;
    g_state_machine.states[state]();
}


void init()
{
    init_state_machine();
    init_ports();
    init_pwm();
    init_int0_interrupt(disabled);



}


int main (void)
{
    init();
    transition_to_state(starting);
    sei();

    set_sleep_mode(SLEEP_MODE_IDLE);
    while(1) {

        sleep_mode();
    }

    return 0;
}
