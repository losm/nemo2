/*
 * Copyright (c) 2020 Maarten Los
 *
 * Distributed under the MIT License, see LICENSE for details.
 * 
 */
#ifndef _NEMO2_UFM_H_
#define _NEMO2_UFM_H_

typedef enum 
{
    led1,
    led2,
    none

} led_t;

typedef enum
{
    rising,
    falling,
    disabled
} sense_t;


void init_int0_interrupt(sense_t sense);
void init_timer_overflow_interrupt(char enable);
void init_ports();
void set_led(led_t led);

void init_pwm();
void set_pwm(led_t led, int ticks_total, int ticks_on);

char on_state_change();
void transition_state();
void transition_to_state(int state);

void init_state_machine();
void init_state_machine_state(int state,
                              int next_state,
                              void (*on_enter_state)());
void init();

#endif /* _NEMO2_UFM_H_ */
